package com.machine.attendancewo.trigger;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

import com.machine.attendancewo.jobpenarikan.AttendancePenarikanJob;


public class AttendancePenarikanJobTrigger {
	public JobDetail getJobDetail() {
		JobKey jobKeyAttPenarikan = new JobKey("GetAttJob", "group1");
		JobDetail attPenarikanJobDetail = JobBuilder.newJob(AttendancePenarikanJob.class)
				.withIdentity(jobKeyAttPenarikan).build();
		return attPenarikanJobDetail;
	}
	
	public Trigger getTrigger() {
		try {
			Trigger jobGetAttTrigger = TriggerBuilder
	    			.newTrigger()
	    			.withIdentity("getAttJobTrigger", "group1")
	    			.withSchedule(
	    					CronScheduleBuilder.cronSchedule("0 0/15 * * * ?"))
	    					.build();
			return jobGetAttTrigger;
		} catch(Exception ex) {
			System.out.println(ex);
			return null;
		}
	}
}
