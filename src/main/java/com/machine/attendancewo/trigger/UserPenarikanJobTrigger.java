package com.machine.attendancewo.trigger;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

import com.machine.attendancewo.jobpenarikan.UserPenarikanJob;

public class UserPenarikanJobTrigger {
	public JobDetail getJobDetail() {
		JobKey jobKeyUserPenarikan = new JobKey("GetUserJob", "groupSatu");
		JobDetail userPenarikanJobDetail = JobBuilder.newJob(UserPenarikanJob.class)
				.withIdentity(jobKeyUserPenarikan).build();
		return userPenarikanJobDetail;
	}
	
	public Trigger getTrigger() {
		String scd =  "0 0/5 * * * ?";
		// format: detik menit jam day-of-month bulan day-of-week year(optional)
        // (s m h doM M dow y)
		
		Trigger jobGetUserTrigger = TriggerBuilder
    			.newTrigger()
    			.withIdentity("getUserJobTrigger", "group1")
    			.withSchedule(
    					CronScheduleBuilder.cronSchedule(scd))
    					.build();
		return jobGetUserTrigger;
	}
}
