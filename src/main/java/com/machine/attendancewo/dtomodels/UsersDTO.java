package com.machine.attendancewo.dtomodels;

import com.machine.attendancewo.models.Status;

public class UsersDTO {
	private Long userPin;
	private StatusDTO status;
	private Long userId;
	private String userName;
	private String userPass;
	
	public UsersDTO() {}
	
	public UsersDTO(Long userPin, StatusDTO status, Long userId, String userName, String userPass) {
		super();
		this.userPin = userPin;
		this.status = status;
		this.userId = userId;
		this.userName = userName;
		this.userPass = userPass;
	}

	public Long getUserPin() {
		return userPin;
	}

	public void setUserPin(Long userPin) {
		this.userPin = userPin;
	}

	public StatusDTO getStatus() {
		return status;
	}

	public void setStatus(StatusDTO status) {
		this.status = status;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPass() {
		return userPass;
	}

	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}
}
