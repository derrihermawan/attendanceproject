package com.machine.attendancewo.dtomodels;

import com.machine.attendancewo.models.Users;

public class AttendanceDTO {
	private Long attendanceId;
	private UsersDTO users;
	private String dateTimeIn;
	private String dateTimeOut;
	private String additionalInformation;
	
	public AttendanceDTO() {}
	
	public AttendanceDTO(Long attendanceId, UsersDTO users, String dateTimeIn, String dateTimeOut,
			String additionalInformation) {
		super();
		this.attendanceId = attendanceId;
		this.users = users;
		this.dateTimeIn = dateTimeIn;
		this.dateTimeOut = dateTimeOut;
		this.additionalInformation = additionalInformation;
	}

	public Long getAttendanceId() {
		return attendanceId;
	}

	public void setAttendanceId(Long attendanceId) {
		this.attendanceId = attendanceId;
	}

	public UsersDTO getUsers() {
		return users;
	}

	public void setUsers(UsersDTO users) {
		this.users = users;
	}

	public String getDateTimeIn() {
		return dateTimeIn;
	}

	public void setDateTimeIn(String dateTimeIn) {
		this.dateTimeIn = dateTimeIn;
	}

	public String getDateTimeOut() {
		return dateTimeOut;
	}

	public void setDateTimeOut(String dateTimeOut) {
		this.dateTimeOut = dateTimeOut;
	}

	public String getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}
}
