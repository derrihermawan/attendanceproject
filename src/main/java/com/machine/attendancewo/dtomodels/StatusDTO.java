package com.machine.attendancewo.dtomodels;

public class StatusDTO {
	private Long statusId;
	private String statusName;
	
	public StatusDTO() {}
	
	public StatusDTO(Long statusId, String statusName) {
		super();
		this.statusId = statusId;
		this.statusName = statusName;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
}
