package com.machine.attendancewo.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.machine.attendancewo.models.Attendance;

@Repository
public interface AttendanceRepository extends JpaRepository<Attendance, Long> {
	//save data to table from machine
	@Modifying
	@Query(value = "INSERT INTO attendance (user_pin, date_time_in, date_time_out) values(:userPin, :dateTimeIn, :dateTimeOut)", nativeQuery = true)	
	@Transactional
	void insertAttendance( @Param("userPin") Long userPin, @Param("dateTimeIn") String dateTimeIn, @Param("dateTimeOut") String dateTimeOut);
	
	//save data to table from machine
	@Modifying
	@Query(value = "INSERT INTO attendance (user_pin, date_time_in, date_time_out, additional_information) values(:userPin, :dateTimeIn, :dateTimeOut, :additionalInformationIn)", nativeQuery = true)	
	@Transactional
	void insertAttendanceWithAddInfo( @Param("userPin") Long userPin, @Param("dateTimeIn") String dateTimeIn, @Param("dateTimeOut") String dateTimeOut, @Param("additionalInformationIn") String additionalInformationIn);

	//get data late
	@Query(value = "SELECT users.user_pin, users.user_name, attendance.date_time_in, attendance.date_time_out, attendance.additional_information from attendance INNER JOIN users ON attendance.user_pin = users.user_pin", nativeQuery = true)
	List<Object> getData();

	//update attendance date keluar
	@Modifying
	@Query(value="UPDATE attendance SET date_time_out = :dateTimeOut, additional_information = :additionalInformationOut WHERE user_pin = :userPin AND date_time_in = :dateTimeIn", nativeQuery = true)
	@Transactional
	void updateDateTimeOut(@Param("userPin") Long userPin, @Param("dateTimeIn") String dateTimeIn, @Param("dateTimeOut") String dateTimeOut,  @Param("additionalInformationOut") String additionalInformationOut);
	
	//update attendance date masuk
	@Modifying
	@Query(value="UPDATE attendance SET date_time_in = :dateTimeIn, additional_information = :additionalInformationIn WHERE user_pin = :userPin AND date_time_out = :dateTimeOut", nativeQuery = true)
	@Transactional
	void updateDateTimeIn(@Param("userPin") Long userPin, @Param("dateTimeIn") String dateTimeIn, @Param("dateTimeOut") String dateTimeOut, @Param("additionalInformationIn") String additionalInformationIn);

}
