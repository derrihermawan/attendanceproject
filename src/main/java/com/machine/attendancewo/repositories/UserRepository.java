package com.machine.attendancewo.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.machine.attendancewo.models.Users;

@Repository
public interface UserRepository extends JpaRepository<Users, Long> {
	
	//insert into table user
	@Modifying
	@Query(value="INSERT INTO users (user_id,  user_pin, user_name, user_pass, user_status) VALUES (:user_id,  :user_pin, :user_name, :user_pass, :user_status);",
    		nativeQuery = true)
	@Transactional
	void saveUser(@Param("user_id") Long user_id, @Param("user_pin") Long user_pin, @Param("user_name") String user_name, @Param("user_pass") String user_pass, @Param("user_status") Long user_status);

	@Query(value="Select * FROM users",
			nativeQuery = true)
	List<Object> getAllUser();
}
