package com.machine.attendancewo.controllers;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.machine.attendancewo.dtomodels.UsersDTO;
import com.machine.attendancewo.models.Users;
import com.machine.attendancewo.penarikan.UserPenarikan;
import com.machine.attendancewo.repositories.UserRepository;

@RestController
@RequestMapping("/api/user")
public class UserController {
	@Autowired
	UserRepository userRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//insert user
	@PostMapping("/insertUser")
	public String save() throws Exception 
	{
	    // save data database
		UserPenarikan userPenarikan = new UserPenarikan();
		String data = userPenarikan.connecting();
		
		String result = null;
		
		// remove the last return character from data
		String newData = data.substring(0, data.length()-1);
		List<Users> listUser = userRepository.findAll();
		List<UsersDTO> listUserDTO = new ArrayList<>();
		for(Users item : listUser) {
			UsersDTO userDTO = modelMapper.map(item, UsersDTO.class);
			listUserDTO.add(userDTO);
		}
		
		// split new data by return character, produce array of row of data
		for (String rows: newData.split("\n"))
		{
			int j = 1;
			long user_pin = 0;
			String user_name = null;
			String user_pass = null;
			long user_id = 0;
			long user_status = 1;
				
			// split row by pipe, produce array of field			
			for (String fields: rows.split("_"))
			{
				switch(j)
				{
					case 1:
						user_id = (long)Integer.parseInt(fields);
						break;
					case 2:				
						if (fields.indexOf("'") != -1) {
							char[] temp = fields.toCharArray();
							temp[fields.indexOf("'")] = '"';
							user_name = String.valueOf(temp);
						} else {
							user_name = fields;
						}				        
						break;
					case 3:						
						user_pass = fields;            // belum enkripsi
						break;
					case 7:
						user_pin = (long)Integer.parseInt(fields);
						break;					
					default:
						break;
				}
				j++;
			}
						
	        try { 
	        	boolean isExist = false;
				for(UsersDTO item : listUserDTO) {
					Long pin = item.getUserPin();
					if(pin == user_pin) {
						isExist = true;
					}
				}
				if(!isExist) {
					userRepository.saveUser(user_id, user_pin, user_name, user_pass, user_status);
			    }
	        	 result = "Raspberry: Berhasil menyimpan data user.";
	        } 
	        catch (Exception e) {	        
	        	result = "Raspberry: Update data user Success.";
	        }
		}
				
		return result;
	}
}
