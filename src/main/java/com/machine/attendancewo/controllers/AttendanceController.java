package com.machine.attendancewo.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
 
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.machine.attendancewo.dtomodels.AttendanceDTO;
import com.machine.attendancewo.interfaces.TimeMachineInterface;
import com.machine.attendancewo.models.Attendance;
import com.machine.attendancewo.penarikan.PenarikanAttendance;
import com.machine.attendancewo.repositories.AttendanceRepository;
import com.mchange.v2.cfg.PropertiesConfigSource.Parse;

@RestController
@RequestMapping("/api/attendance")
public class AttendanceController implements TimeMachineInterface{
	@Autowired 
	AttendanceRepository attendanceRepository;
	
	ModelMapper modelMapper = new ModelMapper();
		
	//insert data attendance from machine
	@PostMapping("/insertAttendance")
	public String save() throws Exception{
		PenarikanAttendance penarikan = new PenarikanAttendance();
		
		String data = penarikan.getDataFromMachine(); 
		String result = null;
		
		// remove the last return character from data
		String newData = data.substring(0, data.length()-1);
		//System.out.println(newData);
		// split new data by return character, produce array of row of data
		for (String rows: newData.split("\n"))
		{
			int j = 1;
			String dateTime = null;
			String dateTimeIn = null;
			String dateTimeOut = null;
			Long userPin = null;

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				
			// split row by pipe, produce array of field
			for (String fields: rows.split("_"))
			{
				switch(j)
				{
					case 1:
						userPin = Long.parseLong(fields);
						break;
					case 2:				
						dateTime = fields;
						Date date = formatter.parse(dateTime);
						int dateHour = date.getHours();
						if(dateHour >= TIMEINMIN && dateHour <= TIMEINMAX) {
							dateTimeIn = dateTime;
						}
						else if(dateHour >= TIMEOUTMIN && dateHour <= TIMEOUTMAX) {
							dateTimeOut = dateTime;
						}
						break;								
					default:
						break;
				}
				j++;
			}

			List<AttendanceDTO> listAttendanceDTO = mappingAttendance();
			boolean add = false;
			boolean addWithTimeIn = false;
			boolean updateTimeIn = false;
			boolean updateTimeOut = false;
			Long attendanceId = null;
			String additionalInformationIn = null;
			String additionalInformationOut = null;

			//jika table attendance masih kosong, maka add
			if(listAttendanceDTO.size() <1) {
				add = true;
			}
			else {
				Date currentDate = new Date();
				Date date = formatter.parse(dateTime);
				int dateYear = date.getYear();
				int dateMonth = date.getMonth();
				int dateDay = date.getDate();
				int dateHour = date.getHours();
				int currentYear = currentDate.getYear();
				int currentMonth = currentDate.getMonth();
				int currentDay = currentDate.getDate();
				int currentHour = currentDate.getHours();
				
				//jika tanggal absen sama dengan tanggal hari ini
				if(dateYear==currentYear && dateMonth==currentMonth && dateDay==currentDay) {	
					for(AttendanceDTO item : listAttendanceDTO) {
						String dateTimeInItem = item.getDateTimeIn();
						String dateTimeOutItem = item.getDateTimeOut();
						long pinItem = item.getUsers().getUserPin();
						//mencari user/karyawan
						if(pinItem==userPin) {
							if(dateTimeInItem!=null && dateTimeOutItem==null && (dateHour >= TIMEINMIN && dateHour <= TIMEINMAX)) {
								add = false;
								updateTimeIn = false;
								updateTimeOut = false;
							}
							else if(dateTimeInItem!=null && dateTimeOutItem==null && (dateHour >= TIMEOUTMIN && dateHour <= TIMEOUTMAX)){
								dateTimeIn = dateTimeInItem;
								updateTimeOut = true;
								add = false;
							}
							else if(dateTimeInItem!=null && dateTimeOutItem==null && (dateHour >= TIMEOUTMIN && dateHour <= TIMEOUTMAX) && currentHour >= TIMEOUTMAX) {
								dateTimeOut = (currentYear+1900)+"-"+(currentMonth+1)+"-"+currentDay+TIMEOUTDEFAULT;
								additionalInformationIn = ADDITIONALINFORMATIONOUT;
								dateTimeIn = dateTimeInItem;
								updateTimeOut = true;
							}
						}
						else {
							if(dateHour >= TIMEOUTMIN && dateHour <= TIMEOUTMAX){
								dateTimeIn = (dateYear+1900)+"-"+(dateMonth+1)+"-"+dateDay+TIMEINDEFAULT;
								additionalInformationIn = ADDITIONALINFORMATIONIN;
								dateTimeOut = dateTimeOutItem;
								addWithTimeIn = true;
							}
							else {
								add = true;
							}
						}
						//cari data kemarin yang null
						if(dateTimeInItem==null && dateTimeOutItem!=null){
							Date dateIn = formatter.parse(dateTimeInItem);
							int year = dateIn.getYear();
							int month = dateIn.getMonth();
							int day = dateIn.getDate();
							if(year==currentYear && month==currentMonth && day==currentDay) {
								dateTimeIn = (year+1900)+"-"+(month+1)+"-"+day+TIMEINDEFAULT;
								attendanceId = item.getAttendanceId();
								additionalInformationIn = ADDITIONALINFORMATIONIN;
								dateTimeOut = dateTimeOutItem;
								updateTimeIn = true;
							}
						}
						else if(dateTimeInItem!=null && dateTimeOutItem==null){
							Date dateOut = formatter.parse(dateTimeOutItem);
							int year = dateOut.getYear();
							int month = dateOut.getMonth();
							int day = dateOut.getDate();
							if(year==currentYear && month==currentMonth && day==currentDay) {
								dateTimeOut = (year+1900)+"-"+(month+1)+"-"+day+TIMEOUTDEFAULT;
								attendanceId = item.getAttendanceId();
								additionalInformationOut = ADDITIONALINFORMATIONOUT;
								dateTimeOut = dateTimeInItem;
								updateTimeOut = true;
							}
						}
					}	
				}
				else {
					for(AttendanceDTO item : listAttendanceDTO) {
						String dateTimeInItem = item.getDateTimeIn();
						String dateTimeOutItem = item.getDateTimeOut();
						int dateItemInYear = 0;
						int dateItemInMonth = 0;
						int dateItemInDate = 0;
						int dateItemOutYear = 0;
						int dateItemOutMonth = 0;
						int dateItemOutDate = 0;
						if(dateTimeInItem!=null) {
							Date dateItemIn = formatter.parse(item.getDateTimeIn());
							dateItemInYear = dateItemIn.getYear();
							dateItemInMonth = dateItemIn.getMonth();
							dateItemInDate = dateItemIn.getDate();
						}
						if(dateTimeOutItem!=null) {
							Date dateItemOut = formatter.parse(item.getDateTimeOut());
							dateItemOutYear = dateItemOut.getYear();
							dateItemOutMonth = dateItemOut.getMonth();
							dateItemOutDate = dateItemOut.getDate();
						}
						
						if((dateYear==dateItemInYear && dateMonth==dateItemInMonth && dateDay==dateItemInDate) || (dateYear==dateItemOutYear && dateMonth==dateItemOutMonth && dateDay==dateItemOutDate)) {
							long pinItem = item.getUsers().getUserPin();
							if(userPin==pinItem) {
								if(dateTimeInItem!=null && dateTimeOutItem!=null) {
									add = false;
									updateTimeIn = false;
									updateTimeOut = false;
								}
								else if(dateTimeInItem==null && dateTimeOutItem!=null) {
									add = false;
									updateTimeIn = false;
									updateTimeOut = false;
								}
								else if(dateTimeInItem!=null && dateTimeOutItem==null && (dateHour >= TIMEINMIN && dateHour <= TIMEINMAX)) {
									add = false;
									updateTimeIn = false;
									updateTimeOut = false;
								}
								else if(dateTimeInItem!=null && dateTimeOutItem==null && (dateHour >= TIMEOUTMIN && dateHour <= TIMEOUTMAX)){
									dateTimeIn = dateTimeInItem;
									updateTimeOut = true;
									add = false;
								}
							}
							else {
								add = true;
								updateTimeIn = false;
								updateTimeOut = false;
							}
						}
						else {
							add = true;
							updateTimeIn = false;
							updateTimeOut = false;
						}
					}
				}
			}
			if(add) {
				try {
					attendanceRepository.insertAttendance(userPin, dateTimeIn, dateTimeOut);
					System.out.println("Add " +add);
					System.out.println(userPin +"_"+ dateTimeIn +"_"+dateTimeOut);
					result = "Raspberry: Berhasil menyimpan data absensi.";
				}catch (Exception e) {
		        	result = "Raspberry: Gagal menyimpan data absensi. " + e.getMessage();
				}
			}
			if(addWithTimeIn) {
				try {
					attendanceRepository.insertAttendanceWithAddInfo(userPin, dateTimeIn, dateTimeOut, additionalInformationIn);
					System.out.println("Add " +add);
					System.out.println(userPin +"_"+ dateTimeIn +"_"+dateTimeOut);
					result = "Raspberry: Berhasil menyimpan data absensi.";
				}catch (Exception e) {
		        	result = "Raspberry: Gagal menyimpan data absensi. " + e.getMessage();
				}
			}
			if(updateTimeOut) {
				try {
					attendanceRepository.updateDateTimeOut(userPin, dateTimeIn, dateTimeOut, additionalInformationOut);
					System.out.println("Update " + updateTimeOut);
					System.out.println(attendanceId +"_"+dateTimeOut);
					result = "Raspberry: Berhasil menyimpan data absensi.";
				}catch (Exception e) {
		        	result = "Raspberry: Gagal menyimpan data absensi. " + e.getMessage();
				}
			}
			if(updateTimeIn) {
				try {
					attendanceRepository.updateDateTimeIn(userPin, dateTimeIn, dateTimeOut, additionalInformationIn);
					System.out.println("Update " + updateTimeIn);
					System.out.println(attendanceId +"_"+dateTimeIn);
					result = "Raspberry: Berhasil menyimpan data absensi.";
				}catch (Exception e) {
		        	result = "Raspberry: Gagal menyimpan data absensi. " + e.getMessage();
				}
			}
		}		
		return result;
	}

	//mapping Present
	private List<AttendanceDTO> mappingAttendance(){
		List<Attendance> listAttendance = attendanceRepository.findAll();
		List<AttendanceDTO> listAttendanceDTO = new ArrayList<AttendanceDTO>();
		for(Attendance attendance : listAttendance) {
			AttendanceDTO attendanceDTO = modelMapper.map(attendance, AttendanceDTO.class);
			listAttendanceDTO.add(attendanceDTO);
		}
		return listAttendanceDTO;
	}
}
