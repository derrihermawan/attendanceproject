package com.machine.attendancewo.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.machine.attendancewo.dtomodels.AttendanceDTO;
import com.machine.attendancewo.models.Attendance;
import com.machine.attendancewo.repositories.AttendanceRepository;
@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("api/reportAttendance")
public class ReportController {
	@Autowired
	AttendanceRepository attendanceRepository;
	ModelMapper modelMapper = new ModelMapper();
	
	private List<AttendanceDTO> mappingAttendance(){
		List<Attendance> listAttendance = attendanceRepository.findAll();
		List<AttendanceDTO> listAttendanceDTO = new ArrayList<AttendanceDTO>();
		for(Attendance attendance : listAttendance) {
			AttendanceDTO attendanceDTO = modelMapper.map(attendance, AttendanceDTO.class);
			listAttendanceDTO.add(attendanceDTO);
		}
		return listAttendanceDTO;
	}
	@GetMapping("/getAllAttendance")
	public Map<String, Object> getAll(){
		Map<String, Object> result = new HashMap<String, Object>();
		try{
			List<Object> listAttendanceDTO = attendanceRepository.getData();
			result.put("Status", "200");
			result.put("Message", "Get all data attendance succesfull");
			result.put("Data", listAttendanceDTO);
		}catch (Exception e) {
			result.put("Status", "500");
			result.put("Message", "Get all data attendance failed");
		}
		
		return result;
		
	}
}
