package com.machine.attendancewo.interfaces;

public interface TimeMachineInterface {
	int TIMEINMIN = 6;
	int TIMEINMAX = 13;
	int TIMEOUTMIN = 15;
	int TIMEOUTMAX = 23;
	String TIMEINDEFAULT = " 09:00:00";
	String TIMEOUTDEFAULT = " 17:00:00";
	String ADDITIONALINFORMATIONIN = "SET TIME IN BY APPLICATION";
	String ADDITIONALINFORMATIONOUT = "SET TIME OUT BY APPLICATION";
}
