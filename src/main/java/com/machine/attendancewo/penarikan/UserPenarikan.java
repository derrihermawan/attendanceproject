package com.machine.attendancewo.penarikan;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import com.machine.attendancewo.helperpenarikan.Parser;

public class UserPenarikan {
	
	public String connecting() throws Exception {
		String result = "";
		Parser parser = new Parser();				
		Socket socket = null;
		String host ="10.10.30.3";
		int port = 80; 
		
		try {  
		    String data = "<GetUserInfo><ArgComKey xsi:type=\"xsd:integer\">"+"0"+"</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetUserInfo>";
		    socket = new Socket(host, port);
		    String path = "/iWsService";
		    BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF8"));
		    String newLine = "\r\n";
		    wr.write("POST " + path + " HTTP/1.0" + newLine);
		    wr.write("Content-Type: text/xml" + newLine);
		    wr.write("Content-Length: " + data.length() + newLine + newLine);	
		    wr.write(data + newLine);
		    wr.write(data);
		    wr.flush();
		    
		    BufferedReader rd = new BufferedReader(new InputStreamReader(socket.getInputStream()));

		    String line = rd.readLine();
		    String buffer = "";
		    do {
		    	buffer += parser.cleanUserLine(line, "<Row>","</Row>");		    	
		    } while ((line = rd.readLine()) != null);
		    
		    // remove header and footer
		    String cleanedData = parser.cleanHeader(buffer, "<GetUserInfoResponse>", "</GetUserInfoResponse>");
		    //System.out.println(cleanedData);
		    
		    result = cleanedData;
		    
		    wr.close();
		    rd.close();
		    
		} catch(IOException e) {
			System.out.println("Raspberry: Gagal koneksi ke mesin fingerprint.");
		}
		socket.close();
		return result;
	}
	
}
