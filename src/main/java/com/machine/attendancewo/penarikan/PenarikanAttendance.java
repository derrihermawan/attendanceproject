package com.machine.attendancewo.penarikan;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import com.machine.attendancewo.helperpenarikan.Parser;

public class PenarikanAttendance {
	
	public String getDataFromMachine() throws Exception{			
		Socket socket = null;
		Parser parser = new Parser();	
		
		
		String host ="10.10.30.3";
		int port = 80;
		String cleanedData ="";
		try {  
		    String data = "<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">"+"0"+"</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
		    socket = new Socket(host, port);
		    String path = "/iWsService";
		    BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF8"));
		    String newLine = "\r\n";
		    wr.write("POST " + path + " HTTP/1.0" + newLine);
		    wr.write("Content-Type: text/xml" + newLine);	
		    wr.write("Content-Length: " + data.length() + newLine + newLine);	
		    wr.write(data + newLine);
		    wr.write(data);
		    wr.flush();
		    
		    BufferedReader rd = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		    System.out.println();
		    String line = rd.readLine();
		    String buffer = "";
		    do {
		    	buffer += parser.cleanAttLine(line, "<Row>","</Row>");		    	
		    } while ((line = rd.readLine()) != null);
		    
		    // remove header and footer
		    cleanedData = parser.cleanHeader(buffer, "<GetAttLogResponse>", "</GetAttLogResponse>");
		 
		    
		    
		}
		catch(IOException e) {
			System.out.println("Raspberry: Gagal koneksi ke mesin fingerprint");
		}
		socket.close();
		return cleanedData;
	}
}
