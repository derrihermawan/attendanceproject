package com.machine.attendancewo.jobpenarikan;

import java.net.HttpURLConnection;
import java.net.URL;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class UserPenarikanJob implements Job{
	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			final String url = "http://localhost:8080/api/user/insertUser";
		    HttpURLConnection httpClient = (HttpURLConnection) new URL(url).openConnection();
	        httpClient.setRequestMethod("POST");
	        httpClient.setRequestProperty("User-Agent", "Mozilla/5.0");
	        httpClient.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	        httpClient.setDoOutput(true);
	        httpClient.getResponseCode();
	        httpClient.connect();
			System.out.println("Penarikan data user berjalan setiap 1 menit.");
			System.out.println("-----------------------------------------------");
		} catch (Exception e) {
			System.out.println("Penarikan data user tidak berjalan. " + e.getMessage());
			System.out.println("-----------------------------------------------");
			e.printStackTrace();
		}		
	}
}
