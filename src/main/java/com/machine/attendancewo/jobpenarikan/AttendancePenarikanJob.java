package com.machine.attendancewo.jobpenarikan;

import java.net.HttpURLConnection;
import java.net.URL;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.web.bind.annotation.RestController;

import com.machine.attendancewo.controllers.AttendanceController;

@RestController
public class AttendancePenarikanJob implements Job {
	AttendanceController attController = new AttendanceController();
	
	/*
	 * Method name	: execute
	 * Method type	: void
	 * Description	: This method implement job execution of GetSaveAttendance class
	 * Return value	: -
	 */	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			final String uri = "http://localhost:8080/api/attendance/insertAttendance";
		    HttpURLConnection httpClient = (HttpURLConnection) new URL(uri).openConnection();
	        httpClient.setRequestMethod("POST");
	        httpClient.setRequestProperty("User-Agent", "Mozilla/5.0");
	        httpClient.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	        httpClient.setDoOutput(true);
	        httpClient.getResponseCode();
	        httpClient.connect();
			System.out.println("Penarikan data absensi berjalan setiap 1 Menit.");
			System.out.println("-----------------------------------------------");
		} catch (Exception e) {
			System.out.println("Penarikan data absensi tidak berjalan. " + e.getMessage());
			System.out.println("-----------------------------------------------");
			e.printStackTrace();
		}		
	}
}
