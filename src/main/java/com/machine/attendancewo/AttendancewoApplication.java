package com.machine.attendancewo;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.machine.attendancewo.trigger.AttendancePenarikanJobTrigger;
import com.machine.attendancewo.trigger.UserPenarikanJobTrigger;


@SpringBootApplication
public class AttendancewoApplication {

	public static void main(String[] args) throws Exception { 
		SpringApplication.run(AttendancewoApplication.class, args);
		AttendancePenarikanJobTrigger attFPM = new AttendancePenarikanJobTrigger();
    	JobDetail getAttJobDetail = attFPM.getJobDetail();
    	Trigger getAttJobTrigger = attFPM.getTrigger();
    	
    	UserPenarikanJobTrigger userFPM = new UserPenarikanJobTrigger();
    	JobDetail userJobDetail = userFPM.getJobDetail();
    	Trigger userTrigger = userFPM.getTrigger();
    	
    	Scheduler scheduler = new StdSchedulerFactory().getScheduler();
		scheduler.start();
		scheduler.scheduleJob(userJobDetail, userTrigger);
		scheduler.scheduleJob(getAttJobDetail, getAttJobTrigger);
		   	
	}

}
